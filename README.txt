Multilevel Select
=================

Modules
-------
ml_select - Declares the Multilevel Select Form Element
ml_select_taxonomy_widget - Adds a widget using the Multilevel Select Form 
Element for taxonomy terms reference fields.

Taxonomy Widget
---------------
At this stage, the ml_select_taxonomy_widget module ONLY creates a new widget 
type that is useable on taxonomy terms reference fields. Just edit create a new
term reference field and set the widget to 'Multilevel select widget'.

When using the widget you'll get two extra options when editing field settings:
 * Height of select boxes: How many lines high the select boxes used by the 
   widget are. Defaults to 6.
 
 * Leaf terms only: Whether or not terms with children can be selected. If set
   only terms that do not have children can be selected. If unset any term can
   be selected.
   
Unlike hierachical select, the Multilevel Select Taxonomy Widget module does 
not support saving the entire lineage of a selected term. This is intentional
and is not something that I will change. However I have written another module
(taxonomy_index_linage) that add the entire lineage of any taxonomy term 
reference field into the taxonomy_index table. 


Using ml_select in your own module
----------------------------------
The ml_select widget has a number of properties. Some of them are optional, 
some of them are required.

#size: How many lines high the generated select boxes are. Defaults to 6.
      
#leaves_only: Whether or not values that have children can be selected. If set 
values that have chilren can not be selected. Defaults to false.

#tree_callback: Required. Callback function used to obtain tree information. 
The function must be of type function($element,$value,$mode). The $mode can be
ML_SELECT_PARENT or ML_SELECT_CHILDREN. The expected function result is as
follows:
 * ML_SELECT_PARENT: Return the value of the parent of $value. Top level values
   must return 0. 
 * ML_SELECT_CHILDREN: Return an array of the children of $value. Array is 
   names as they should appear in the select boxes index by value. If a value
   has no children then an empty result should be returned.
If there is an error return FALSE.

#tree_arguments: Array of additional arguments to pass to the #tree_callback
function. These arguments are passed after the other required arguments.


Using ml_select with the taxonomy widgets tree callback in your own module
--------------------------------------------------------------------------
If you want to use the ml_select_taxonomy_widget modules implementation of
tree_callback in your own form then set the ml_select form element array 
values:
'#tree_callback' =>	'ml_select_taxonomy_widget_callback',
'#tree_arguments' => array($vocabulary_id)
